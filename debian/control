Source: wcalc
Section: math
Priority: optional
Maintainer: Josue Ortega <josue@debian.org>
Build-Depends: debhelper (>= 11),
 bison,
 flex,
 libmpfr-dev,
 libgmp3-dev,
 libreadline-dev,
 texinfo (>= 5.2.0~),
 dpkg-dev (>= 1.16.1~)
Standards-Version: 4.1.3
Homepage: http://w-calc.sourceforge.net/
Vcs-Browser: https://salsa.debian.org/debian/wcalc
Vcs-Git: https://salsa.debian.org/debian/wcalc.git

Package: wcalc
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Flexible command-line scientific calculator
 Wcalc is a very capable calculator. It has standard functions
 (sin, asin, and sinh for example, in either radians or degrees),
 many pre-defined constants (pi, e, c, etc.), support for using
 variables, "active" variables, a command history, hex/octal/binary
 input and output, unit conversions, embedded comments, and an
 expandable expression entry field. It evaluates expressions using
 the standard order of operations.
 .
 Wcalc uses intuitive expressions. For example, Wcalc will evaluate:
 5sin 4!-7*2(4%6)^2 to be -221.96631678
