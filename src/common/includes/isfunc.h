#ifndef WCALC_ISFUNC
#define WCALC_ISFUNC

#include "explain.h"

extern const struct name_with_exp funcs[];
int isfunc(const char *str);
#endif
/* vim:set expandtab: */
